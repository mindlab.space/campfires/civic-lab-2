# Campfire for civic-lab-2

Campfire for EEG based BCI.

## How to Install

**Pre-requisite:**
- OpenBCI_GUI software.
- OpenBCI Ganglion or Cyton device or any other BCI device compatible with the OpenBCI_GUI software.
- Python 3

1. Clone the source code repository with GIT:
```
$ git clone https://gitlab.com/mindlab.space/campfires/civic-lab-2.git
```

2. Install node_modules
```  
$ cd osc-bridge-udp-ws
$ npm install
```

3. Start the "udp - websocket" server.
```  
$ node .
```
The "udp - websocket" server is listening at ```127.0.0.1:7400```

4. Start OpenBCI_GUI 

5. Setup OpnBCI_GUI networking widget
- Protocol: OSC  
***Stream 1 settings:**
    - Data Type: BandPower
    - IP: 127.0.0.1
    - Port: 7400
    - Address /openbci
    - Filters: 1-ON

6. Press th button "Start Data Stream" in the OpenBCI_GUI.

7. Press the "Start" button of the OSC widget

![OpenBCI_GUI](./docs/openbci_gui.png "OpenBCI_GUI")

8. Wear the BCI
At least 1 sensor is needed.   
One sensor setup: AFZ or OZ   
Two sensors setup: AF7 and AF8   

![10-20 system for EEG](./docs/10-20_system_for_EEG.png "10-20 system for EEG")



## How to have fun

### Can you relax

An application to practice with relaxed mental state.

1. Start the webserver
In another terminal
```
$ cd ./GUIs/can_you_relax
$ ./webserver.sh
```

2. Open a browser at the address: ```http://localhost:8000```

3. Double-click on the browser's window.

4. Follow the instructions.

