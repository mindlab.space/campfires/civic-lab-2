let cnv_width = 1920;
let cnv_height = 1080;
let yPos = 0;

/**
 * Initial setup
 */
function setup() {
    createCanvas(cnv_width, cnv_height);
    background(200);
}

/**
 * Draw loop
 */
function draw() {

    background(200);

    yPos = yPos - 1;
    if (yPos < 0) {
        yPos = height;
    }
    line(0, yPos, width, yPos);

}