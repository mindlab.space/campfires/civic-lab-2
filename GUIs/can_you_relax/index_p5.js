/**
 * Neuro Adventure
 * 
 * A fanstastic game made with p5js.
 * This file is a p5js sketch file.
 *
 */


/**
 * Preload media
 */
function preload() 
{
    img_background = loadImage('assets/on_game_start_img_background.jpg');

    txt_instruction = loadStrings('assets/intruction.txt');

    audio_pay_attention = createAudio('assets/voice_pay_attention_to_your_breath_now.mp3');

}

/**
 * Initial setup
 */
function setup() {

    app_status = 'on_app_start';

    setup_on_app_start_is_done = false;
    setup_on_app_intro_is_done = false;
    setup_on_app_preroll_is_done = false;
    setup_on_app_relax_exercise_is_done = false;
    setup_on_app_over_is_done = false;

    cnv_width = 1920;
    cnv_height = 1080;
    cnv = createCanvas(cnv_width, cnv_height);

    framerate = 30;
    frameRate(framerate);

    background(200);

    bci_channel_1 = false;
    // bci_channel_2 = false;
    // bci_channel_3 = false;
    // bci_channel_4 = false;

}

/**
 * Draw loop
 */
function draw() {
    evaluate_app_status();
}


const evaluate_app_status = () => {

    switch(app_status) {
        case 'on_app_start':
            setup_on_app_start();
            draw_on_app_start();
            break;
        case 'on_app_intro':
            setup_on_app_intro();
            draw_on_app_intro();
            break;
        case 'on_app_preroll':
            setup_on_app_preroll();
            draw_on_app_preroll();
            break;
        case 'app_relax_exercise':
            setup_on_app_relax_exercise();
            draw_on_app_relax_exercise();
            break;
        case 'app_over':
            setup_on_app_over();
            draw_on_app_over();
            break;

        default:
            console.error('case not defined', app_status);
    } 

}

const change_app_status_on_app_intro = () => {
    app_status = 'on_app_intro';
}

const change_app_status_on_app_preroll = () => {
    app_status = 'on_app_preroll';
}

const change_app_status_on_app_relax_exercise = () => {
    app_status = 'app_relax_exercise';
}

const change_app_status_on_app_over = () => {
    app_status = 'app_over';
}



/**
 * Initial setup for "on_game_start" app status
 */
const setup_on_app_start = () => {

    if ( !setup_on_app_start_is_done ) {

        cnv.doubleClicked( change_app_status_on_app_intro );
        setup_on_app_over_is_done = false;

        setup_on_app_start_is_done = true;

    }

}

const setup_on_app_intro = () => {

    if ( !setup_on_app_intro_is_done ) {

        cnv.remove(); // remove elements and listeners
        cnv = createCanvas(cnv_width, cnv_height);
        if( typeof button_doitagain !== "undefined") {
            button_doitagain.remove();
        }

        duration = 1;
        slider_duration = createSlider(1, 3, 1, 1);
        slider_duration.position(40, 80);
        slider_duration.style('width', '200px');

        feedback_audio_amp = 4
        slider_feedback_audio = createSlider(0, 10, 4, 1);
        slider_feedback_audio.position(40, 200);
        slider_feedback_audio.style('width', '200px');


        bci_channel_1 = true
        checkbox_bci_channel_1 = createCheckbox('', 1);
        checkbox_bci_channel_1.position(40, 310);
        checkbox_bci_channel_1.changed(checkbox_bci_channel_1_changed);

        // checkbox_bci_channel_2 = createCheckbox('', 1);
        // checkbox_bci_channel_2.position(40, 330);
        // checkbox_bci_channel_2.changed(checkbox_bci_channel_2_changed);

        // checkbox_bci_channel_3 = createCheckbox('', 1);
        // checkbox_bci_channel_3.position(40, 350);
        // checkbox_bci_channel_3.changed(checkbox_bci_channel_3_changed);

        // checkbox_bci_channel_4 = createCheckbox('', 1);
        // checkbox_bci_channel_4.position(40, 370);
        // checkbox_bci_channel_4.changed(checkbox_bci_channel_4_changed);


        button_start = createButton('START');
        button_start.addClass('button');
        button_start.addClass('is-success');
        button_start.position(width/2 -70, height/2 +200);
        button_start.size(140, 50);
        button_start.mousePressed(change_app_status_on_app_preroll);

        setup_on_app_over_is_done = false;

        setup_on_app_intro_is_done = true;
        
    }
}

function checkbox_bci_channel_1_changed() {
    if (this.checked()) {
        bci_channel_1 = true;
    } else {
        bci_channel_1 = false;
    }
}
// function checkbox_bci_channel_2_changed() {
//     if (this.checked()) {
//         bci_channel_2 = true;
//     } else {
//         bci_channel_2 = false;
//     }
// }
// function checkbox_bci_channel_3_changed() {
//     if (this.checked()) {
//         bci_channel_3 = true;
//     } else {
//         bci_channel_3 = false;
//     }
// }
// function checkbox_bci_channel_4_changed() {
//     if (this.checked()) {
//         bci_channel_4 = true;
//     } else {
//         bci_channel_4 = false;
//     }
// }


const setup_on_app_preroll = () =>  {
    if ( !setup_on_app_preroll_is_done ) { 

        cnv.remove(); // remove elements and listeners
        cnv = createCanvas(cnv_width, cnv_height);
        slider_duration.remove();
        slider_feedback_audio.remove();
        button_start.remove();

        checkbox_bci_channel_1.remove();
        // checkbox_bci_channel_2.remove();
        // checkbox_bci_channel_3.remove();
        // checkbox_bci_channel_4.remove();

        duration = slider_duration.value();
        feedback_audio_amp = slider_feedback_audio.value();

        timer = 10;
        timer_start = false;

        audio_played = false;

        setup_on_app_preroll_is_done = true;
    }
}

const setup_on_app_relax_exercise = () => {
    if ( !setup_on_app_relax_exercise_is_done ) {
        
        cnv.remove(); // remove elements and listeners
        cnv = createCanvas(cnv_width, cnv_height);

        // defined in modules/mindlab_space/osc.js
        init_oscWebSocket();

        // it will contain the alpha graph lines
        alpha_lines_dx_up = [];
        alpha_lines_sx_up = [];

        // init alpha value (used in case osc-message is not incoming)
        alpha_val = 0;

        let channel_active;
        if ( bci_channel_1 ) {
            channel_active = 1;
        }
        msgParser = new OSCOpenBCIDataParser(channel_active);


        // Init timer
        if( typeof duration === "undefined" ) {
            duration = 3;
        }
        timer = duration * 60;
        timer_start = false;


        // Audio feedback
        if (typeof feedback_audio_amp === "undefined" ) { 
            feedback_audio_amp = 4;
        }
        let type = 'white';
        noise = new p5.Noise(type);
        filter = new p5.LowPass();
        reverb = new p5.Reverb();
        reverb.set(3,10);
        reverb.amp(feedback_audio_amp);
        noise.disconnect();
        filter.disconnect();
        noise.connect(filter);
        filter.connect(reverb);
        noise.start();
        
        // Alpha points(yo!)
        alpha_points = 0;

        setup_on_app_relax_exercise_is_done = true;

    }
}

const setup_on_app_over = () => {
    
    if( !setup_on_app_over_is_done ) {

        cnv.remove(); // remove elements and listeners
        cnv = createCanvas(cnv_width, cnv_height);

        if (typeof alpha_points === "undefined" ) { 
            alpha_points = 0;
        }

        noise.stop();

        setup_on_app_start_is_done = false;
        setup_on_app_intro_is_done = false;
        setup_on_app_preroll_is_done = false;
        setup_on_app_relax_exercise_is_done = false;

        button_doitagain = createButton('TRY AGAIN');
        button_doitagain.addClass('button');
        button_doitagain.addClass('is-success');
        button_doitagain.position(width/2 -70, height/2 +200);
        button_doitagain.size(140, 50);
        button_doitagain.mousePressed(change_app_status_on_app_intro);

        setup_on_app_over_is_done = true;
    }
}


const draw_on_app_start = () => {

    image(img_background, 0, 0);

    textSize(196);
    textAlign(CENTER);
    fill(249, 189, 189);
    strokeWeight(2);
    stroke(21, 18, 85);
    text('Can You Relax?', width/2, height/4);

}

const draw_on_app_intro = () => {

    image(img_background, 0, 0);

    draw_on_app_intro_draw_instruction();
    draw_on_app_intro_draw_duration_input();
}

const draw_on_app_intro_draw_instruction = () => {
        
    textSize(32);
    textAlign(RIGHT);
    fill(255);
    strokeWeight(0);
    stroke(0);

    text(txt_instruction[0], width -40, 70);
    textSize(20);
    text(txt_instruction[1], width -40, 140,);
    text(txt_instruction[2], width -40, 200);
    text(txt_instruction[3], width -40, 230);
    text(txt_instruction[4], width -40, 260);
    text(txt_instruction[5], width -40, 290);
    text(txt_instruction[6], width -40, 320);
    text(txt_instruction[7], width -40, 380);
    text(txt_instruction[8], width -40, 410);
    text(txt_instruction[9], width -40, 460);
}

const draw_on_app_intro_draw_duration_input = () => {

    textSize(14);
    textAlign(LEFT);
    fill(255);
    strokeWeight(0);
    stroke(0);
    text('Duration in minutes', 40, 60);

    slider_duration_val = slider_duration.value();
    textSize(14);
    textAlign(LEFT);
    fill(255);
    strokeWeight(0);
    stroke(0);
    text(slider_duration_val, 137, 120);


    textSize(14);
    textAlign(LEFT);
    fill(255);
    strokeWeight(0);
    stroke(0);
    text('Audio feedback volume', 40, 180);

    slider_feedback_audio_val = slider_feedback_audio.value();
    textSize(14);
    textAlign(LEFT);
    fill(255);
    strokeWeight(0);
    stroke(0);
    text(slider_feedback_audio_val, 137, 240);


    textSize(14);
    textAlign(LEFT);
    fill(255);
    strokeWeight(0);
    stroke(0);
    text('Active BCI channels', 40, 295);
    text('BCI channel 1', 60, 325);
    // text('BCI channel 2', 60, 345);
    // text('BCI channel 3', 60, 365);
    // text('BCI channel 4', 60, 385);

}


const draw_on_app_preroll = () => {
    
    image(img_background, 0, 0);
    draw_on_app_preroll_draw_timer();

}

const draw_on_app_preroll_draw_timer = () => {

    if (frameCount % framerate == 0 && timer > 0){
        if ( !timer_start ) {
            timer_start = true;
        } else {
            timer--;
        }
    }

    if ( 2 == timer && !audio_played) {
        audio_played = true;
        play_audio(audio_pay_attention, slider_feedback_audio_val * 0.1);
    }

    if ( 0 == timer ) {
        change_app_status_on_app_relax_exercise();
    }

    textSize(600);
    textAlign(CENTER);
    fill(255, 255, 255, 127);
    strokeWeight(0);
    stroke(0);
    text(timer, width/2, height/2 + 200);

}

const draw_on_app_relax_exercise = () => {

    image(img_background, 0, 0);

    // remove element when out of screen
    if ( alpha_lines_dx_up.length == width/2) {
        alpha_lines_dx_up.pop();
    }
    // remove element when out of screen
    if ( alpha_lines_sx_up.length == width/2) {
        alpha_lines_sx_up.pop();
    }
    
    // add new line to the graph
    let lw = new LineWrap( 
        width/2, 
        height/2 + 26 + alpha_val, 
        width/2, 
        height/2 + 26 - alpha_val
    );
    alpha_lines_dx_up.unshift(lw);
    lw = new LineWrap( 
        width/2, 
        height/2 + 26 + alpha_val, 
        width/2, 
        height/2 + 26 - alpha_val
    );
    alpha_lines_sx_up.unshift(lw);

    draw_alpha_lines_graph();

    draw_on_app_relax_exercise_draw_timer();

    // Audio feedback
    let amp = map(alpha_val, 0, 800, 0, feedback_audio_amp);
    amp = constrain(amp, 0, feedback_audio_amp);
    noise.amp(amp);

    let freq = map(alpha_val, 0, 800, 0, 10000);
    freq = constrain(freq, 0, 22050);
    filter.freq(freq);

    draw_on_app_relax_exercise_draw_alpha_points();
    

}

const draw_on_app_relax_exercise_draw_timer = () => {
    

    if (frameCount % framerate == 0 && timer > 0){
        if ( !timer_start ) {
            timer_start = true;
        } else {
            timer--;
        }
    }

    if ( timer == 0 ) {
        change_app_status_on_app_over();
    }

    textSize(28);
    textAlign(CENTER);
    fill(255, 255, 255, 127);
    strokeWeight(0);
    stroke(0);
    text(timer, width/2, height/2 +400);

}

const draw_alpha_lines_graph = () => {


    strokeWeight(2);

    alpha_lines_dx_up.forEach(function(lw_dx, index, a) {
        lw_dx.x1 = lw_dx.x1 + 1;
        lw_dx.x2 = lw_dx.x2 + 1;
        let stroke_alpha_dx = map(lw_dx.x1, width/2, width, 240, 0);
        stroke(130, 167, 153, stroke_alpha_dx);    
        lw_dx.display();
    });

    alpha_lines_sx_up.forEach(function(lw_sx, index, a) {
        lw_sx.x1 = lw_sx.x1 - 1;
        lw_sx.x2 = lw_sx.x2 - 1;
        let stroke_alpha_sx = map(width - lw_sx.x1, width/2, width, 240, 0);
        stroke(130, 167, 153, stroke_alpha_sx);
        lw_sx.display();
    });

}

const draw_on_app_relax_exercise_draw_alpha_points = () => {
    alpha_points = alpha_points + round( alpha_val / 150 );
    textSize(18);
    textAlign(CENTER);
    fill(255, 255, 255, 127);
    strokeWeight(0);
    stroke(0);
    text('alpha points', width/2, height/2 +450);
    textSize(28);
    text(alpha_points, width/2, height/2 +488);
}


const onOSCSocketMessage = (oscMsg) => {
    
    let alpha_val_received = msgParser.parseOSCMessage(oscMsg);
    if( typeof alpha_val_received !== "undefined" ) {
        alpha_val = alpha_val_received * 80; 
    }
}


const draw_on_app_over = () => {

    image(img_background, 0, 0);

    textSize(48);
    textAlign(CENTER);
    fill(255, 255, 255, 127);
    strokeWeight(0);
    stroke(0);
    text('Awesome!', width/2, 100);

    textSize(28);
    text('you made', width/2, 190 );
    textSize(48);
    text(alpha_points, width/2, 250);
    textSize(28);
    text('alpha points', width/2, 290)

}