/**
 * Data acquisition library for OpenBCI_GUI OSC input data stream
 * 
 * At the moment works only with one channel amd for BandPower Alpha.
 *
 */

/**
 * Class OSCOpenBCIDataParser
 * Parse OSC messages coming from OpenBCI_GUI software.
 */
class OSCOpenBCIDataParser
{
    bci_channel;

    constructor( bci_channel ) 
    {
        this.bci_channel = 1;// bci_channel;
    }

    parseOSCMessage( oscMsg ) {

        // only collect messages that belogs to the channel in use
        if ( this.bci_channel == oscMsg['args'][0].value ) {
            // only return alpha band value 
            return oscMsg['args'][3].value;
        }

    }
}