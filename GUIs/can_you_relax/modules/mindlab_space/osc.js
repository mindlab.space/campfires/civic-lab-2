
const init_oscWebSocket = () => {
    
    oscWebSocket = new osc.WebSocketPort({
        url: "ws://localhost:8081",
        metadata: true
    });    
    
    oscWebSocket.on("ready", onOSCSocketOpen);
    oscWebSocket.on("message", onOSCSocketMessage);
    oscWebSocket.open();
}


const onOSCSocketOpen = () => {
    console.log("Socket open with success!");
}


// const onOSCSocketMessage = (oscMsg) => {
//     console.log("message received", oscMsg);
// }