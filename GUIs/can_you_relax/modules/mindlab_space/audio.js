/**
 * P5.js library extension for audio.
 * 
 **/


/**
 * play_audio()
 * 
 * @param {*} audio: an audio object created with createAudio() 
 * @param {*} volume: audio volume output
 */
const play_audio = (audio, volume) => {
    audio.volume(volume);
    audio.play();
}